package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        String tempString = "";
        for (String string : args) {
            tempString += string + " ";
        }
        return tempString;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
